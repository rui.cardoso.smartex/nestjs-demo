import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { faker } from '@faker-js/faker';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    jest.setTimeout(0)

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('Happy path testing', async () => {
    //POST
    const body = {
      title: faker.lorem.word(),
      description: faker.lorem.word(),
      price: 1.123,
      category: faker.lorem.word(),
    }
    let response = await request(app.getHttpServer()).post('/products').send(body);
    expect(response.statusCode).toEqual(201);
    expect(response.body.data).toMatchObject({
      uuid: expect.any(String),
      ...body
    });

    //GET
    response = await request(app.getHttpServer()).get(`/products?category=${body.category}&name=${body.title}`);
    expect(response.statusCode).toEqual(200);
    expect(response.body.data.length).toBeGreaterThanOrEqual(1);
    const product = response.body.data[0];
    expect(product).toMatchObject({
      uuid: expect.any(String),
      title: expect.any(String),
      description: expect.any(String),
      price: expect.any(Number),
      category: expect.any(String)
    });

    //PUT
    const updateBody = {
      title: faker.lorem.word(),
      category: faker.lorem.word(),
    }
    response = await request(app.getHttpServer()).put(`/products/${product.uuid}`).send(updateBody);
    expect(response.statusCode).toEqual(200);
    expect(response.body.data).toMatchObject({
      uuid: expect.any(String),
      ...body,
      ...updateBody
    });

    //DELETE
    response = await request(app.getHttpServer()).delete(`/products/${product.uuid}`);
    expect(response.statusCode).toEqual(200);
    expect(response.body.data).toMatchObject({
      uuid: expect.any(String),
      ...body,
      ...updateBody
    });
  });

  describe('Negative tests', () => {
    it('/products (POST) - validation errors', async () => {
      //POST
      const body = {
        title: faker.lorem.word(),
        description: faker.lorem.word(),
        price: faker.lorem.word(),
      }
      let response = await request(app.getHttpServer()).post('/products').send(body);
      expect(response.statusCode).toEqual(400);
      expect(response.body.message).toEqual([
        'price must be a positive number',
        'price must be a number conforming to the specified constraints',
        'category should not be empty',
        'category must be a string'
      ]);
    });

    it('/products/:productId (GET) - invalid UUID', async () => {
      const response = await request(app.getHttpServer()).get('/products/foo-bar');
      expect(response.statusCode).toEqual(400);
      expect(response.body.message).toEqual([
        "productId must be a UUID"
      ]);
    });

    it('/products/:productId (GET) - Product not found', async () => {
      const response = await request(app.getHttpServer()).get('/products/4c9ea91d-0c23-40d7-a43d-c2a273c5061b');
      expect(response.statusCode).toEqual(400);
      expect(response.body.message).toEqual("Product not found!");
    });
  });
});