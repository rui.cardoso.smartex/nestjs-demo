FROM node:16.14.2-alpine3.14

WORKDIR /usr/src/app

COPY package*.json /usr/src/app/

RUN npm install

COPY . /usr/src/app/

EXPOSE 3333

CMD npm run start