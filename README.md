# Smartex Code Challenge

## Tech stack
* NestJs [http://nestjs.com](http://nestjs.com)
* LowDB [https://www.npmjs.com/package/lowdb](https://www.npmjs.com/package/lowdb)

## Swagger Documentation
[http://localhost:3333/api](http://localhost:3333/api)

## Docker Installation && Run
```bash
docker build -f dockerfile -t smartex-code-challenge .
docker run -p 3333:3333 --rm smartex-code-challenge
```

## Local Installation && Run

### Installation

```bash
$ npm install
```

### Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

### Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
