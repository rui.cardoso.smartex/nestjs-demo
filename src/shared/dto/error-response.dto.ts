import { ApiProperty } from '@nestjs/swagger';
import { BaseResponse } from "./base-response.dto";

export class ErrorResponse<T> extends BaseResponse<T> {
  @ApiProperty({ example: 'Bad request', description: 'The error description' })
  error: string = '';

  constructor(error: string, message: string | string[]) {
    super(null, message)
    this.error = error;
    this.success = false;
  }

  toObject() {
    return {
      ...super.toObject(),
      ...{
        error: this.error,
      }
    }
  }
}