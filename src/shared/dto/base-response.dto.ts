import { ApiProperty } from '@nestjs/swagger';

export class BaseResponse<T> {
  @ApiProperty({ example: true, description: 'The type of response' })
  success: boolean = false;

  @ApiProperty({ example: 200, description: 'The status code of response' })
  statusCode: number = 200;

  @ApiProperty({ example: {}, description: 'The data content' })
  data: T;

  @ApiProperty({ example: 'Success', description: 'The response message' })
  message: string | string[];

  constructor(data: T, message: string | string[] = '') {
    this.data = data;
    this.message = message;
    this.success = true;
  }

  toObject() {
    return {
      statusCode: this.statusCode,
      success: this.success,
      message: this.message,
      data: this.data
    }
  }
}