import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Request, Response } from 'express';
import { ErrorResponse } from '../dto';

@Catch()
export class ApiExceptionFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    // const status = exception.getStatus();

    const errorResponse = new ErrorResponse<any>(
      exception.message,
      exception?.response?.message,
    );

    response.json({
      ...errorResponse.toObject(),
      ...{
        method: request.method || null,
        // statusCode: status,
        timestamp: new Date().toISOString(),
        path: request.url,
      },
    });
  }
}
