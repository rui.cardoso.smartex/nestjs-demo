import { Field, ID, ObjectType } from '@nestjs/graphql';
import { ProductModel } from '.';

@ObjectType({ description: 'Order' })
export class OrderModel {
  @Field((type) => ID)
  uuid: string;

  @Field({ nullable: true })
  total: string;

  @Field({ nullable: true })
  date: number;

  @Field((type) => [ProductModel], { nullable: true })
  products: [ProductModel];
}
