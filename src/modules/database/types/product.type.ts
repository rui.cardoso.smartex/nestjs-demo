import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'Manufacturer' })
export class ManufacturerModel {
  @Field({ nullable: true })
  name: string;

  @Field({ nullable: true })
  location: string;

  @Field({ nullable: true })
  date: number;
}
@ObjectType({ description: 'Product' })
export class ProductModel {
  @Field((type) => ID)
  uuid: string;

  @Field({ nullable: true })
  title: string;

  @Field({ nullable: true })
  description: string;

  @Field({ nullable: true })
  price: number;

  @Field({ nullable: true })
  category: string;

  @Field((type) => ManufacturerModel, { nullable: true })
  manufacturer: ManufacturerModel;
}
