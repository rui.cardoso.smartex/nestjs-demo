import { HttpException, Injectable } from '@nestjs/common';
import { Low } from '@commonify/lowdb';
import { faker } from '@faker-js/faker';
import { LowDbLoader } from './low-db.loader';
import { ProductModel } from '../types';
import {
  CreateProductDto,
  GetProductsFilterDto,
  UpdateProductDto,
} from 'src/modules/products/dto';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class ProductsService extends LowDbLoader<any> {
  constructor() {
    super('products.json', { products: [] });
  }

  async getAllProducts(filter: GetProductsFilterDto): Promise<ProductModel[]> {
    const db: Low<any> = await this.getDbInstance();

    let data = db.data.products;

    if (filter.name) {
      data = data.filter((elem) => elem.title == filter.name);
    }
    if (filter.category) {
      data = data.filter((elem) => elem.category == filter.category);
    }

    return data;
  }

  async getProduct(uuid: string): Promise<ProductModel> {
    const db = await this.getDbInstance();
    const data = db.data.products.filter((elem) => elem.uuid === uuid);
    if (data.length === 0) {
      throw new HttpException('Product not found!', 400);
    }

    return data[0];
  }

  async deleteProduct(uuid: string): Promise<ProductModel> {
    const db = await this.getDbInstance();
    const index = db.data.products.findIndex((elem) => elem.uuid === uuid);
    if (index === -1) {
      throw new HttpException('Product not found!', 400);
    }

    const product: ProductModel = db.data.products[index];
    db.data.products.splice(index, 1);
    db.write();

    return product;
  }

  async createProduct(
    createProductDto: CreateProductDto,
  ): Promise<ProductModel> {
    const db = await this.getDbInstance();
    const product: ProductModel = {
      ...createProductDto,
      uuid: uuidv4(),
      manufacturer: {
        name: faker.name.firstName(),
        location: faker.address.city(),
        date: faker.datatype.number(),
      },
    };
    db.data.products.push(product);
    db.write();

    return product;
  }

  async updateProduct(
    uuid: string,
    updateProductDto: UpdateProductDto,
  ): Promise<ProductModel> {
    const db = await this.getDbInstance();
    const index = db.data.products.findIndex((elem) => elem.uuid === uuid);
    if (index === -1) {
      throw new HttpException('Product not found!', 400);
    }

    const newProduct = { ...db.data.products[index], ...updateProductDto };
    db.data.products[index] = newProduct;

    db.write();

    return newProduct;
  }
}
