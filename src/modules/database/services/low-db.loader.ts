import { join } from 'path'
import { Low, JSONFile } from '@commonify/lowdb';
import * as fs from 'fs';

export class LowDbLoader<T> {
  private _dbInstance: Low<any>;

  constructor(private _file: string, private _initState: T) { }

  public async getDbInstance(): Promise<Low<T>> {
    if (this._dbInstance) {
      return Promise.resolve(this._dbInstance);
    }

    const file = join(__dirname, `../../../../storage/${this._file}`)
    if (!fs.existsSync(file)) {
      throw 'File not found: ' + file
    }

    this._dbInstance = new Low<T>(new JSONFile<T>(file));
    await this._dbInstance.read();
    this._dbInstance.data = this._dbInstance.data || this._initState

    return this._dbInstance;
  }
}