import { HttpException, Injectable } from '@nestjs/common';
import { Low } from '@commonify/lowdb';
import { LowDbLoader } from './low-db.loader';
import { OrderModel } from '../types';

@Injectable()
export class OrdersService extends LowDbLoader<any> {
  constructor() {
    super('orders.json', { orders: [] });
  }

  async getAllOrders(): Promise<OrderModel[]> {
    const db: Low<any> = await this.getDbInstance();

    return db.data.orders;
  }

  async getOrder(uuid: string): Promise<OrderModel> {
    const db = await this.getDbInstance();
    const data = db.data.orders.filter((elem) => elem.uuid === uuid);
    if (data.length === 0) {
      throw new HttpException('Order not found!', 400);
    }

    return data[0];
  }
}
