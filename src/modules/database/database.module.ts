import { Global, Module } from '@nestjs/common';
import { ProductsService, OrdersService } from './services';

@Global()
@Module({
  providers: [ProductsService, OrdersService],
  exports: [ProductsService, OrdersService],
})
export class DatabaseModule {}
