import { CacheModule, Module } from '@nestjs/common';
import { OrdersResolver } from './orders.resolver';
import { OrdersController } from './orders.controller';

@Module({
  imports: [
    CacheModule.register({
      ttl: 5,
      max: 100,
    }),
  ],
  controllers: [OrdersController],
  providers: [OrdersResolver],
  exports: [],
})
export class OrdersModule {}
