import { Args, Query, Resolver } from '@nestjs/graphql';
import { NotFoundException } from '@nestjs/common';
import { OrderModel } from '../database/types';
import { OrdersService } from '../database/services';

@Resolver((of) => OrderModel)
export class OrdersResolver {
  constructor(private ordersService: OrdersService) {}

  @Query((returns) => OrderModel)
  async order(@Args('id') id: string): Promise<OrderModel> {
    const order = await this.ordersService.getOrder(id);
    if (!order) {
      throw new NotFoundException(id);
    }
    return order;
  }

  @Query((returns) => [OrderModel])
  async allOrders(): Promise<OrderModel[]> {
    const orders = await this.ordersService.getAllOrders();
    return orders;
  }
}
