import {
  ClassSerializerInterceptor,
  Param,
  UseInterceptors,
  Controller,
  Get,
  ValidationPipe,
  CacheInterceptor,
} from '@nestjs/common';
import {
  ApiOperation,
  ApiBadRequestResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { BaseResponse, ErrorResponse } from '../../shared/dto';
import { OrdersService } from '../database/services';
import { OrderModel } from '../database/types';
import { GetOrderDto } from './dto/';

@ApiTags('Orders')
@ApiBadRequestResponse({
  description: 'Bad Request',
  type: ErrorResponse,
})
@Controller('orders')
export class OrdersController {
  constructor(private ordersService: OrdersService) {}

  @Get('/')
  @ApiOperation({ summary: 'Get orders' })
  @ApiOkResponse({
    description: 'The orders',
    type: BaseResponse,
  })
  @UseInterceptors(CacheInterceptor)
  @UseInterceptors(ClassSerializerInterceptor)
  async getOrders(): Promise<BaseResponse<OrderModel[]>> {
    return new BaseResponse<OrderModel[]>(
      await this.ordersService.getAllOrders(),
    );
  }

  @Get('/:orderId')
  @ApiOperation({ summary: 'Get orders By Uuid' })
  @ApiOkResponse({
    description: 'The order',
    type: BaseResponse,
  })
  @UseInterceptors(CacheInterceptor)
  @UseInterceptors(ClassSerializerInterceptor)
  async getProductByUuid(
    @Param(new ValidationPipe()) params: GetOrderDto,
  ): Promise<BaseResponse<OrderModel>> {
    return new BaseResponse<OrderModel>(
      await this.ordersService.getOrder(params.orderId),
    );
  }
}
