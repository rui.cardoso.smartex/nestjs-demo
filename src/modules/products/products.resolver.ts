import { Args, Query, Resolver } from '@nestjs/graphql';
import { NotFoundException } from '@nestjs/common';
import { ProductModel } from '../database/types';
import { ProductsService } from '../database/services';
import { GetProductsFilterDto } from './dto';

@Resolver((of) => ProductModel)
export class ProductsResolver {
  constructor(private productsService: ProductsService) {}

  @Query((returns) => ProductModel)
  async product(@Args('id') id: string): Promise<ProductModel> {
    const product = await this.productsService.getProduct(id);
    if (!product) {
      throw new NotFoundException(id);
    }
    return product;
  }

  @Query((returns) => [ProductModel])
  async allProducts(): Promise<ProductModel[]> {
    const products = await this.productsService.getAllProducts(
      new GetProductsFilterDto(),
    );
    return products;
  }
}
