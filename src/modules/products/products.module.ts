import { CacheModule, Module } from '@nestjs/common';
import { ProductsController } from './products.controller';
import { ProductsResolver } from './products.resolver';

@Module({
  imports: [
    CacheModule.register({
      ttl: 5,
      max: 100,
    }),
  ],
  controllers: [ProductsController],
  providers: [ProductsResolver],
  exports: [],
})
export class ProductsModule {}
