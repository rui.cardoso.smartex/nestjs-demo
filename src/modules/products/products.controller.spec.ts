import { ProductsController } from './products.controller';
import { faker } from '@faker-js/faker';
import { ProductsService } from '../database/services';
import { ProductModel } from '../database/types';
import { BaseResponse } from '../../shared/dto';

describe('ProductsController', () => {
  let productsController: ProductsController;
  let productsService: ProductsService;

  beforeEach(() => {
    productsService = new ProductsService();
    productsController = new ProductsController(productsService);
  });

  describe('getProductByUuid', () => {
    it('should return a single Product', async () => {
      const uuid = faker.datatype.uuid();
      const result: ProductModel = {
        title: faker.lorem.words(),
        description: faker.lorem.words(),
        price: faker.datatype.float(),
        category: faker.lorem.word(),
        uuid
      };
      jest.spyOn(productsService, 'getProduct').mockImplementation(() => Promise.resolve(result));
      
      expect(await productsController.getProductByUuid({productId : uuid})).toEqual(new BaseResponse<ProductModel>(result));
    });
  });
});
