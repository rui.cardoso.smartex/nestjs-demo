import {
  Body,
  ClassSerializerInterceptor,
  Param,
  Query,
  UseInterceptors,
  Controller,
  Get,
  Post,
  ValidationPipe,
  Put,
  Delete,
  CacheInterceptor,
} from '@nestjs/common';
import {
  ApiOperation,
  ApiBadRequestResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { BaseResponse, ErrorResponse } from '../../shared/dto';
import { ProductsService } from '../database/services';
import { ProductModel } from '../database/types';
import {
  CreateProductDto,
  GetProductDto,
  GetProductsFilterDto,
  UpdateProductDto,
} from './dto/index';

@ApiTags('Products')
@ApiBadRequestResponse({
  description: 'Bad Request',
  type: ErrorResponse,
})
@Controller('products')
export class ProductsController {
  constructor(private productsService: ProductsService) {}

  @Get('/')
  @ApiOperation({ summary: 'Get/Filter products' })
  @ApiOkResponse({
    description: 'The Filtered products',
    type: BaseResponse,
  })
  @UseInterceptors(CacheInterceptor)
  @UseInterceptors(ClassSerializerInterceptor)
  async getProducts(
    @Query(new ValidationPipe()) filter: GetProductsFilterDto,
  ): Promise<BaseResponse<ProductModel[]>> {
    return new BaseResponse<ProductModel[]>(
      await this.productsService.getAllProducts(filter),
    );
  }

  @Get('/:productId')
  @ApiOperation({ summary: 'Get products By Uuid' })
  @ApiOkResponse({
    description: 'The product',
    type: BaseResponse,
  })
  @UseInterceptors(CacheInterceptor)
  @UseInterceptors(ClassSerializerInterceptor)
  async getProductByUuid(
    @Param(new ValidationPipe()) params: GetProductDto,
  ): Promise<BaseResponse<ProductModel>> {
    return new BaseResponse<ProductModel>(
      await this.productsService.getProduct(params.productId),
    );
  }

  @Put('/:productId')
  @ApiOperation({ summary: 'Update product By Uuid' })
  @ApiOkResponse({
    description: 'The product',
    type: BaseResponse,
  })
  @UseInterceptors(ClassSerializerInterceptor)
  async update(
    @Param(new ValidationPipe()) params: GetProductDto,
    @Body(new ValidationPipe()) updateProductDto: UpdateProductDto,
  ): Promise<BaseResponse<ProductModel>> {
    return new BaseResponse<ProductModel>(
      await this.productsService.updateProduct(
        params.productId,
        updateProductDto,
      ),
      'Product Updated',
    );
  }

  @Delete('/:productId')
  @ApiOperation({ summary: 'Delete product By Uuid' })
  @ApiOkResponse({
    description: 'The product',
    type: BaseResponse,
  })
  @UseInterceptors(ClassSerializerInterceptor)
  async deleteProductByUuid(
    @Param(new ValidationPipe()) params: GetProductDto,
  ): Promise<BaseResponse<ProductModel>> {
    return new BaseResponse<ProductModel>(
      await this.productsService.deleteProduct(params.productId),
      'Product Deleted',
    );
  }

  @Post('/')
  @ApiOperation({ summary: 'Create a new Product' })
  @ApiOkResponse({
    description: 'The product',
    type: BaseResponse,
  })
  @UseInterceptors(ClassSerializerInterceptor)
  async create(
    @Body(new ValidationPipe()) createProductDto: CreateProductDto,
  ): Promise<BaseResponse<ProductModel>> {
    return new BaseResponse<ProductModel>(
      await this.productsService.createProduct(createProductDto),
      'Product Saved',
    );
  }
}
