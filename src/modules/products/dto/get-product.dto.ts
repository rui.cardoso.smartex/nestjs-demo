import { IsString, IsNotEmpty, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class GetProductDto {
  @ApiProperty({
    example: 'ef13796a-d015-4fbf-942d-8f889d9f4909',
    description: 'The product Uuid',
  })
  @IsString()
  @IsNotEmpty()
  @IsUUID(4)
  productId: string;
}
