import { IsString, IsNumber, IsPositive, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateProductDto {
  @ApiProperty({ example: 'Foo Bar', description: 'The product name' })
  @IsString()
  @IsNotEmpty()
  title: string;

  @ApiProperty({ 
    example: 'Nam ut sem quis elit porta convallis. Quisque a sagittis.', 
    description: 'The product description' 
  })
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty({ example: 123.321, description: 'The product price' })
  @IsNumber()
  @IsPositive()
  price: number;

  @ApiProperty({ example: 'Electronics', description: 'The product category' })
  @IsString()
  @IsNotEmpty()
  category: string;
}
