import { IsString, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class GetProductsFilterDto {
  @ApiProperty({ example: 'Product name', description: 'The product name', required: false })
  @IsString()
  @IsOptional()
  name: string;

  @ApiProperty({ example: 'Product category', description: 'The product category', required: false })
  @IsString()
  @IsOptional()
  category: string;
}
