import { IsString, IsNumber, IsPositive, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateProductDto {
  @ApiProperty({ example: 'Foo Bar', description: 'The product name', required: false })
  @IsString()
  @IsOptional()
  title: string;

  @ApiProperty({ 
    example: 'Nam ut sem quis elit porta convallis. Quisque a sagittis.', 
    description: 'The product description',
    required: false
  })
  @IsString()
  @IsOptional()
  description: string;

  @ApiProperty({ example: 123.321, description: 'The product price', required: false })
  @IsNumber()
  @IsPositive()
  @IsOptional()
  price: number;

  @ApiProperty({ example: 'Electronics', description: 'The product category', required: false  })
  @IsString()
  @IsOptional()
  category: string;
}
